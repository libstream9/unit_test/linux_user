#include <stream9/linux/group.hpp>

#include <boost/test/unit_test.hpp>

#include <stream9/json.hpp>
#include <stream9/linux/error.hpp>
#include <stream9/strings/equal.hpp>

namespace testing {

namespace lx { using namespace stream9::linux; }
namespace json { using namespace stream9::json; }
namespace str { using namespace stream9::strings; }

BOOST_AUTO_TEST_SUITE(getgrnam_)

    BOOST_AUTO_TEST_SUITE(non_reentrant_variant_)

        BOOST_AUTO_TEST_CASE(success_)
        {
            auto* gr = lx::getgrnam("root");

            BOOST_REQUIRE(gr != nullptr);

            BOOST_TEST(gr->gr_gid == 0);
        }

        BOOST_AUTO_TEST_CASE(not_found_)
        {
            auto* gr = lx::getgrnam("xxxxx");

            BOOST_TEST(gr == nullptr);
        }

    BOOST_AUTO_TEST_SUITE_END() // non_reentrant_variant_

    BOOST_AUTO_TEST_SUITE(reentrant_variant_)

        BOOST_AUTO_TEST_CASE(found_)
        {
            std::vector<char> buf;
            buf.resize(lx::getgr_buffer_size());

            auto o_gr = lx::getgrnam("root", buf);

            BOOST_REQUIRE(o_gr);
            BOOST_TEST(o_gr->gr_gid == 0);
        }

        BOOST_AUTO_TEST_CASE(not_found_)
        {
            std::vector<char> buf;
            buf.resize(lx::getgr_buffer_size());

            auto o_gr = lx::getgrnam("xxxxxx", buf);

            BOOST_TEST(!o_gr);
        }

        BOOST_AUTO_TEST_CASE(error_)
        {
            char buf[1];

            BOOST_CHECK_EXCEPTION(
                lx::getgrnam("root", buf),
                stream9::error,
                [&](auto& e) {
                    BOOST_TEST(e.why() == lx::erange);
                    return true;
                });
        }

    BOOST_AUTO_TEST_SUITE_END() // reentrant_variant_

BOOST_AUTO_TEST_SUITE_END() // getgrnam_

BOOST_AUTO_TEST_SUITE(getgrgid_)

    BOOST_AUTO_TEST_SUITE(non_reentrant_variant_)

        BOOST_AUTO_TEST_CASE(success_)
        {
            auto* gr = lx::getgrgid(0);

            BOOST_REQUIRE(gr != nullptr);

            BOOST_TEST(str::equal(gr->gr_name, "root"));
        }

        BOOST_AUTO_TEST_CASE(not_found_)
        {
            auto* gr = lx::getgrgid(INT_MAX);

            BOOST_TEST(gr == nullptr);
        }

    BOOST_AUTO_TEST_SUITE_END() // non_reentrant_variant_

    BOOST_AUTO_TEST_SUITE(reentent_variant_)

        BOOST_AUTO_TEST_CASE(found_)
        {
            std::vector<char> buf;
            buf.resize(lx::getgr_buffer_size());

            auto o_gr = lx::getgrgid(0, buf);

            BOOST_REQUIRE(o_gr);
            BOOST_TEST(str::equal(o_gr->gr_name, "root"));
        }

        BOOST_AUTO_TEST_CASE(not_found_)
        {
            std::vector<char> buf;
            buf.resize(lx::getgr_buffer_size());

            auto o_gr = lx::getgrgid(INT_MAX, buf);

            BOOST_TEST(!o_gr);
        }

        BOOST_AUTO_TEST_CASE(error_)
        {
            char buf[1];

            BOOST_CHECK_EXCEPTION(
                lx::getgrgid(0, buf),
                stream9::error,
                [&](auto& e) {
                    BOOST_TEST(e.why() == lx::erange);
                    return true;
                });
        }

    BOOST_AUTO_TEST_SUITE_END() // reentent_variant_

BOOST_AUTO_TEST_SUITE_END() // getgrgid_

} // namespace testing
