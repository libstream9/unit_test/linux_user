#include <stream9/linux/password.hpp>

#include <boost/test/unit_test.hpp>

#include <stream9/json.hpp>
#include <stream9/linux/error.hpp>
#include <stream9/strings/equal.hpp>

namespace testing {

namespace lx { using namespace stream9::linux; }
namespace json { using namespace stream9::json; }
namespace str { using namespace stream9::strings; }

BOOST_AUTO_TEST_SUITE(getpwnam_)

    BOOST_AUTO_TEST_SUITE(non_reentrant_variant_)

        BOOST_AUTO_TEST_CASE(success_)
        {
            auto* pw = lx::getpwnam("root");

            BOOST_REQUIRE(pw != nullptr);

            BOOST_TEST(pw->pw_uid == 0);
            BOOST_TEST(pw->pw_gid == 0);
        }

        BOOST_AUTO_TEST_CASE(not_found_)
        {
            auto* pw = lx::getpwnam("xxxxx");

            BOOST_TEST(pw == nullptr);
        }

    BOOST_AUTO_TEST_SUITE_END() // non_reentrant_variant_

    BOOST_AUTO_TEST_SUITE(reentrant_variant_)

        BOOST_AUTO_TEST_CASE(found_)
        {
            std::vector<char> buf;
            buf.resize(lx::getpw_buffer_size());

            auto o_pw = lx::getpwnam("root", buf);

            BOOST_REQUIRE(o_pw);
            BOOST_TEST(o_pw->pw_uid == 0);
            BOOST_TEST(o_pw->pw_gid == 0);
        }

        BOOST_AUTO_TEST_CASE(not_found_)
        {
            std::vector<char> buf;
            buf.resize(lx::getpw_buffer_size());

            auto o_pw = lx::getpwnam("xxxxxx", buf);

            BOOST_TEST(!o_pw);
        }

        BOOST_AUTO_TEST_CASE(error_)
        {
            char buf[1];

            BOOST_CHECK_EXCEPTION(
                lx::getpwnam("root", buf),
                stream9::error,
                [&](auto& e) {
                    BOOST_TEST(e.why() == lx::erange);
                    return true;
                });
        }

    BOOST_AUTO_TEST_SUITE_END() // reentrant_variant_

BOOST_AUTO_TEST_SUITE_END() // getpwnam_

BOOST_AUTO_TEST_SUITE(getpwuid_)

    BOOST_AUTO_TEST_SUITE(non_reentrant_variant_)

        BOOST_AUTO_TEST_CASE(success_)
        {
            auto* pw = lx::getpwuid(0);

            BOOST_REQUIRE(pw != nullptr);

            BOOST_TEST(str::equal(pw->pw_name, "root"));
        }

        BOOST_AUTO_TEST_CASE(not_found_)
        {
            auto* pw = lx::getpwuid(INT_MAX);

            BOOST_TEST(pw == nullptr);
        }

    BOOST_AUTO_TEST_SUITE_END() // non_reentrant_variant_

    BOOST_AUTO_TEST_SUITE(reentent_variant_)

        BOOST_AUTO_TEST_CASE(found_)
        {
            std::vector<char> buf;
            buf.resize(lx::getpw_buffer_size());

            auto o_pw = lx::getpwuid(0, buf);

            BOOST_REQUIRE(o_pw);
            BOOST_TEST(str::equal(o_pw->pw_name, "root"));
        }

        BOOST_AUTO_TEST_CASE(not_found_)
        {
            std::vector<char> buf;
            buf.resize(lx::getpw_buffer_size());

            auto o_pw = lx::getpwuid(INT_MAX, buf);

            BOOST_TEST(!o_pw);
        }

        BOOST_AUTO_TEST_CASE(error_)
        {
            char buf[1];

            BOOST_CHECK_EXCEPTION(
                lx::getpwuid(0, buf),
                stream9::error,
                [&](auto& e) {
                    BOOST_TEST(e.why() == lx::erange);
                    return true;
                });
        }

    BOOST_AUTO_TEST_SUITE_END() // reentent_variant_

BOOST_AUTO_TEST_SUITE_END() // getpwuid_

} // namespace testing
